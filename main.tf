resource "aws_cloudwatch_metric_alarm" "this" {
  for_each = var.create_metric_alarm && length(keys(var.dimensions)) > 0 ? var.dimensions : {}

  alarm_name        = format("%s%s", var.alarm_name, each.key)
  alarm_description = var.alarm_description
  actions_enabled   = var.actions_enabled

  alarm_actions             = var.alarm_actions
  ok_actions                = var.ok_actions
  insufficient_data_actions = var.insufficient_data_actions

  comparison_operator = var.comparison_operator
  evaluation_periods  = var.evaluation_periods
  threshold           = var.threshold
  unit                = var.unit

  datapoints_to_alarm                   = var.datapoints_to_alarm
  treat_missing_data                    = var.treat_missing_data
  evaluate_low_sample_count_percentiles = var.evaluate_low_sample_count_percentiles

  metric_name        = var.metric_names[each.key]
  namespace          = var.namespace
  period             = var.period
  statistic          = var.statistic

  dimensions = each.value

  tags = var.tags
}
